import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Observer, Subscription} from 'rxjs';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
   numberObsSubscription: Subscription;
   customObsSubscription: Subscription;
  constructor() { }

  ngOnInit() {
    // first simple observable
    // map() => it maps the data into new observable with any transformation of our choice
    // map takes function as an argument and return transform data
    const myNumbers = Observable.interval(1000)
      .map((data: number) => {
        return data * 2 ;
        }
      );
    this.numberObsSubscription = myNumbers.subscribe(
      (no: number) => {
        console.log('number is' + no);
      }
    );
    // create our own observable
    const myObservable = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {observer.next('first package'); }, 2000);
      setTimeout(() => {observer.error('this is not done'); }, 8000);
      setTimeout(() => {observer.next('second package'); }, 4000);
      setTimeout(() => {observer.complete(); }, 6000);
      setTimeout(() => {observer.next('third package'); }, 9000);
    });
    this.customObsSubscription = myObservable.subscribe(
      (data: string) => {console.log(data); },
      (error: string) => {console.log(error); },
      () => {console.log('completed'); }
    );
  }
  ngOnDestroy(): void {
    this.customObsSubscription.unsubscribe();
    this.numberObsSubscription.unsubscribe();
  }

}
